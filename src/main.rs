use std::error::Error;

use xcb::x::Rectangle;

pub struct Window {
    pub window: xcb::x::Window,
    pub picture: xcb::render::Picture,
    pub attributes: xcb::x::GetWindowAttributesReply,
}

impl Window {
    pub fn new(window: xcb::x::Window, connection: &xcb::Connection) -> Option<Self> {
        let picture: xcb::render::Picture = connection.generate_id();
        let formats = {
            let cookie = connection.send_request(&xcb::render::QueryPictFormats {});
            connection
                .wait_for_reply(cookie)
                .unwrap()
                .formats()
                .iter()
                .map(|x| x.id())
                .collect::<Vec<_>>()
        };
        let mut found = false;
        for format in formats.iter() {
            let cookie = connection.send_request_checked(&xcb::render::CreatePicture {
                pid: picture,
                drawable: xcb::x::Drawable::Window(window),
                format: *format,
                value_list: &[xcb::render::Cp::SubwindowMode(
                    xcb::x::SubwindowMode::IncludeInferiors,
                )],
            });
            let check = connection.check_request(cookie);
            if check.is_ok() {
                found = true;
                break;
            }
        }
        let attributes = {
            let cookie = connection.send_request(&xcb::x::GetWindowAttributes{
                window,
            });
            let resp = connection.wait_for_reply(cookie);
            resp.ok()?
        };
        if !found {
            return None;
        }
        Some(Self { window, picture, attributes })
    }
}

pub struct Compositor {
    connection: xcb::Connection,
    screen_num: i32,
    screen: xcb::x::ScreenBuf,

    pub windows: Vec<Window>,
}

impl Compositor {
    pub fn paint_root(&self) {
        /*use xcb::render::*;

        let pid: Picture = self.connection.generate_id();
        self.connection.send_request(&CreatePicture {
            pid,
            drawable: xcb::x::Drawable::Window(self.screen.root()),
            format: ,
            value_list: todo!(),
        });

        self.connection.send_request(&FillRectangles {
            op: PictOp::Atop,
            dst: todo!(),
            color: Color {
                red: 0x8080,
                green: 0,
                blue: 0,
                alpha: 0xffff,
            },
            rects: &[Rectangle {
                x: 0,
                y: 0,
                width: 200,
                height: 200,
            }],
        });*/

        self.connection.send_request(&xcb::x::MapWindow {
            window: self.screen.root(),
        });
        let root_gc: xcb::x::Gcontext = self.connection.generate_id();

        let root_drawable = xcb::x::Drawable::Window(self.screen.root());
        self.connection.send_request(&xcb::x::CreateGc {
            cid: root_gc,
            drawable: root_drawable,
            value_list: &[
                xcb::x::Gc::Foreground(self.screen.black_pixel()),
                xcb::x::Gc::GraphicsExposures(false),
            ],
        });
        /*self.connection.send_request(&xcb::x::PolyRectangle {
            drawable: root_drawable,
            gc: root_gc,
            rectangles: &[xcb::x::Rectangle {
                x: 0,
                y: 0,
                width: 200,
                height: 200,
            }],
        });*/

        let root_region: xcb::xfixes::Region = self.connection.generate_id();
        {
            let cookie = self
                .connection
                .send_request(&xcb::xfixes::CreateRegionFromWindow {
                    region: root_region,
                    window: self.screen.root(),
                    kind: xcb::shape::Sk::Bounding,
                });
        }

        let root_window_wrapped = Window::new(self.screen.root(), &self.connection).unwrap();
        for window in self.windows.iter() {
            let window_drawable = xcb::x::Drawable::Window(window.window);
            let window_gc: xcb::x::Gcontext = self.connection.generate_id();
            self.connection.send_request(&xcb::x::CreateGc {
                cid: window_gc,
                drawable: window_drawable,
                value_list: &[
                    xcb::x::Gc::Foreground(self.screen.black_pixel()),
                    xcb::x::Gc::GraphicsExposures(false),
                ],
            });
            let geometry = {
                let cookie = self.connection.send_request(&xcb::x::GetGeometry {
                    drawable: window_drawable,
                });
                let resp = self.connection.wait_for_reply(cookie);
                if resp.is_err() {
                    println!("mmm");
                    continue;
                }
                resp.unwrap()
            };

            /*let cookie = self.connection.send_request_checked(&xcb::x::CopyArea {
                src_drawable: window_drawable,
                dst_drawable: root_drawable,
                gc: window_gc,
                src_x: 0,
                src_y: 0,
                dst_x: geometry.x(),
                dst_y: geometry.y(),
                width: geometry.width(),
                height: geometry.height(),
            });*/
            let cookie = self
                .connection
                .send_request_checked(&xcb::render::Composite {
                    op: xcb::render::PictOp::Src,
                    src: window.picture,
                    mask: xcb::render::PICTURE_NONE,
                    dst: root_window_wrapped.picture,
                    src_x: 0,
                    src_y: 0,
                    mask_x: 0,
                    mask_y: 0,
                    dst_x: geometry.x() + geometry.border_width() as i16,
                    dst_y: geometry.y() + geometry.border_width() as i16,
                    width: geometry.width(),
                    height: geometry.height(),
                });
            let check = self.connection.check_request(cookie);
            println!("{:?} {geometry:?}", check);
            if check.is_err() {
                println!("eeeee {}", check.err().unwrap());
            }

            /*self.connection.send_request(&xcb::x::PolyRectangle {
                drawable: root_drawable,
                gc: root_gc,
                rectangles: &[xcb::x::Rectangle {
                    x: geometry.x(),
                    y: geometry.y(),
                    width: geometry.width(),
                    height: geometry.height(),
                }],
            });*/
        }
    }

    pub fn new() -> xcb::Result<Self> {
        let (connection, screen_num) = xcb::Connection::connect(None)?;

        let setup = connection.get_setup();
        let screen = setup.roots().nth(screen_num as usize).unwrap().to_owned();

        let windows = {
            let cookie = connection.send_request(&xcb::x::QueryTree {
                window: screen.root(),
            });
            let reply = connection.wait_for_reply(cookie)?;
            //reply.children()
            Vec::from_iter(
                reply
                    .children()
                    .iter()
                    .cloned()
                    .map(|w| Window::new(w, &connection))
                    .filter(|w| w.is_some())
                    .map(Option::unwrap),
            )
        };

        //redirect all windows
        {
            let cookie = connection.send_request(&xcb::composite::RedirectSubwindows {
                window: screen.root(),
                update: xcb::composite::Redirect::Manual,
            });
        }

        Ok(Self {
            connection,
            screen_num,
            screen,

            windows,
        })
    }

    pub fn add_window(&mut self, window: xcb::x::Window) {
        if let Some(window) = Window::new(window, &self.connection) {
            self.windows.push(window);
        }
    }

    pub fn destroy_window(&mut self, window: xcb::x::Window) {
        if let Some(pos) = self.windows.iter().position(|w| w.window == window) {
            self.windows.remove(pos);
        }
    }

    pub fn main_loop(&mut self) {
        let cookie = self.connection.send_request_checked(&xcb::x::ChangeWindowAttributes{
            window: self.screen.root(),
            value_list: &[
                xcb::x::Cw::EventMask(xcb::x::EventMask::SUBSTRUCTURE_NOTIFY.union(xcb::x::EventMask::EXPOSURE))
            ]
        });
        self.connection.check_request(cookie).unwrap();
        loop {
            let event = self.connection.poll_for_event();
            if event.is_err() || event.as_ref().unwrap().is_none() {
                self.paint_root();
                continue;
            }
            match event.unwrap().unwrap() {
                xcb::Event::X(xcb::x::Event::CreateNotify(cn)) => {
                    self.add_window(cn.window());
                },
                xcb::Event::X(xcb::x::Event::DestroyNotify(dn)) => {
                    self.destroy_window(dn.window());
                },
                _ => {}
            }
        }
    }
}

fn main() {
    let mut comp = Compositor::new().unwrap();
    comp.main_loop();
}
